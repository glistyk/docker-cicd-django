FROM python:3.10-slim

ENV PYTHONUNBUFFERED 1

WORKDIR /code
RUN mkdir db
COPY . /code
RUN pip install -r requirements.txt && \
    chmod +x /code/init.sh

EXPOSE 8000

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000

